export class ProviderData {

    id: number;
    providerName: string;
    mainAddress: string;
    city: string;
    state: string;
    postalCode: string;
    mainPhoneNumber: string;
    mainEmail: string;
    password: string;

    constructor(_id?:number, _providerName?:string, _mainAddress?:string, _city?:string, _state?:string, 
        _postalCode?:string, _mainPhoneNumber?:string, _mainEmail?:string, _password?:string) {
        
        this.id = _id || 0;
        this.providerName = _providerName || "";
        this.mainAddress = _mainAddress || "";
        this.city = _city || "";
        this.state = _state || "";
        this.postalCode = _postalCode || "";
        this.mainPhoneNumber = _mainPhoneNumber || "";
        this.mainEmail = _mainEmail || "";
        this.password = _password || "";
    }

}