export class CovidData {

    id: number;
    state: string;
    tot_cases: number;
    tot_death: number;

    constructor(_id?:number, _state?:string, _tot_cases?:number, _tot_death?:number) {
        this.id = _id || 0;
        this.state = _state || "";
        this.tot_cases = _tot_cases || 0;
        this.tot_death = _tot_death || 0;        
    }

}
