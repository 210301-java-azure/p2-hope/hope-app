import { Location } from "./location";
import { Provider } from "./provider";

export class Appointment {
    apptid: number;
    provider: Provider;
    date: Date;
    time: string;
    location: Location;
    vaccineMfr: string;

    constructor(_apptid?:number,_provider?: Provider, _date?: Date, _time?: string, _location?: Location, _vaccineMfr?: string){
        this.apptid = _apptid || 0;
        this.provider = _provider || new Provider();
        this.date = _date || new Date();
        this.time = _time || "";
        this.location = _location || new Location;
        this.vaccineMfr = _vaccineMfr || "";
    }

    
}
