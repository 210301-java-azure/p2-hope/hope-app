import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  readyToSubmit: boolean = false;
  user: User = new User();
  registrationForm:any;
  message: String = "";

  get userEmail(){
    return this.registrationForm?.get("userEmail");
  }
  get password(){
    return this.registrationForm?.get("password");
  }
  get passwordConfirmation(){
    return this.registrationForm?.get("passwordConfirmation");
  }
 
  registerUser(){
    this.user.email = this.userEmail.value;
    this.user.password = this.password.value;
    this.authService.attemptRegister(this.user).subscribe((response)=>{
      let token = response.headers.get("Authorization");
      sessionStorage.setItem("Authorization",token);
      this.authService.updateLoginStatus(true);
      this.matDialog.closeAll();
      this.router.navigate(['home']);
    },
    (response)=>{
      if(response.status===400){
        this.message = "An account with this E-Mail Address already exists. If you have forgotten your password, please try resetting it."
      }
    }
    );

  }
  
  constructor(private authService:AuthService, private router: Router, private matDialog: MatDialog) { }

  ngOnInit(): void {
    this.registrationForm = new FormGroup({
      userEmail: new FormControl("",[
        Validators.required,
        Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/)
      ]),
      password: new FormControl("",[
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(/^(?=.{3,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/)
      ]),
      passwordConfirmation: new FormControl("",[
        Validators.required, confirmPasswords
      ])
    });
  }

}

function confirmPasswords(control: AbstractControl) {
  let group = control.parent;
  if (group?.get("password")?.value !== group?.get("passwordConfirmation")?.value) {
    return { passwordMismatch: true };
  }
  return null;
}