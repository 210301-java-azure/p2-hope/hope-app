import { Component, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import {Location} from 'src/app/models/location';
import {LocationService} from 'src/app/services/location.service';

import { ProviderData } from '../../models/provider-data';
import { ProviderRegistrationService } from 'src/app/services/provider-registration.service';




@Component({
  selector: 'app-location-form',
  templateUrl: './location-form.component.html',
  styleUrls: ['./location-form.component.css']
})
export class LocationFormComponent implements OnInit {





  newLocation: Location = new Location();

  currentProvider: ProviderData = new ProviderData();


  message: string = "";

  constructor(private locationModal: MatDialog, private locationService:LocationService, private providerRegistrationService:ProviderRegistrationService) { 

  }

  ngOnInit(): void { 
    this.getProvider();
  }

  addLocation() {


    if(this.newLocation.onsite == true) {
      this.retrieveProviderInformation();    
    } else {
      this.newLocation.provider.id = this.currentProvider.id;
    }
    
    if(!this.newLocation.siteName || !this.newLocation.address
      || !this.newLocation.city || !this.newLocation.state || !this.newLocation.postalCode 
      || !this.newLocation.contactPerson || !this.newLocation.contactPersonPhoneNumber 
      || !this.newLocation.provider) {
        
        this.message = 'Unsuccessful. Incomplete form.';
        return;
    }
    
    

    this.locationService.addLocation(this.newLocation)
      .subscribe(locationData => {
        console.log("locationData: " + locationData)
        this.newLocation = locationData
        console.log("this.newLocation: " + this.newLocation)
      });

  }

  retrieveProviderInformation(): void {

    this.newLocation.provider = this.currentProvider;
    this.newLocation.onsite = true;
    this.newLocation.siteName = "Main Site";
    this.newLocation.address = this.currentProvider.mainAddress;
    this.newLocation.city = this.currentProvider.city;
    this.newLocation.state = this.currentProvider.state;
    this.newLocation.postalCode = this.currentProvider.postalCode;  
  }

  getProvider():void {
    let id = Number(sessionStorage.getItem("provider"));
    this.providerRegistrationService.getProviderById(id)
    .subscribe(data => {
      this.currentProvider = data;
    });

  }

}
