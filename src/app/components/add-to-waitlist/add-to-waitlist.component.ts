import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Patient } from 'src/app/models/patient';
import { PatientPortalService } from 'src/app/services/patient-portal.service';
import { PatientPortalComponent } from '../patient-portal/patient-portal.component';

@Component({
  selector: 'app-add-to-waitlist',
  templateUrl: './add-to-waitlist.component.html',
  styleUrls: ['./add-to-waitlist.component.css']
})
export class AddToWaitlistComponent implements OnInit {

  patient: Patient = new Patient();
  message: string = "";

  constructor(private patientPortalService: PatientPortalService) {
  }

  ngOnInit(): void {
  }

  preExistingConditionChoices: String[] = ["No", "Yes"];
  maxDistancesList: Number[] = [10, 25, 50];

  registrationForm = new FormGroup({
    firstName: new FormControl("",[
      Validators.required,
      Validators.pattern(/^[A-za-z]/)
    ]),
    lastName: new FormControl("",[
      Validators.required,
      Validators.pattern(/^[A-za-z]/)
    ]),
    patientEmail: new FormControl("",[
      Validators.required,
      Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)
    ]), 
    phoneNumber: new FormControl("", [
      Validators.required,
      Validators.pattern(/^(?:\d{1}\s)?\(?(\d{3})\)?-?\s?(\d{3})-?\s?(\d{4})/)
    ]),
    zipcode: new FormControl("", [
      Validators.required,
      Validators.pattern(/^[0-9]{5}(-[0-9]{4})?/)
    ]),
    dateOfBirth: new FormControl("",[
      Validators.required
    ]),
    preExistingCondition: new FormControl("",[
      Validators.required
    ]),
    maxDistance: new FormControl("", [
      Validators.required
    ])
  });

  get firstName() {
    return this.registrationForm?.get("firstName");
  }

  get lastName() {
    return this.registrationForm?.get("lastName");
  }

  get patientEmail(){
    return this.registrationForm?.get("patientEmail");
  }

  get phoneNumber() {
    return this.registrationForm?.get("phoneNumber");
  }

  get zipcode() {
    return this.registrationForm?.get("zipcode");
  }

  get dateOfBirth() {
    return this.registrationForm?.get("dateOfBirth");
  }

  get preExistingCondition() {
    return this.registrationForm?.get("preExistingCondition")
  }

  get maxDistance() {
    return this.registrationForm?.get("maxDistance");
  }

  changeCondition(e: any) {
    this.preExistingCondition?.setValue(e.target.value, {
      onlySelf: true
    })
  }

  addToVaccinationWaitlist() { // takes in a Patient and adds the patient to waitlist for vaccination
    this.patient.firstName = this.firstName?.value;
    this.patient.lastName = this.lastName?.value;
    this.patient.patientEmail = this.patientEmail?.value;
    this.patient.phoneNumber = this.phoneNumber?.value;
    this.patient.zipcode = this.zipcode?.value;
    this.patient.dateOfBirth = this.dateOfBirth?.value;
    const condition = this.preExistingCondition?.value;
    if (condition == "Yes") {
      this.patient.preExistingCondition = true;
    } else {
      this.patient.preExistingCondition = false;
    }
    this.patient.maxDistance = this.maxDistance?.value;

    this.patientPortalService.addPatient(this.patient).subscribe(()=>{this.message = "Successfully registered for Vaccination"},
    ()=>{this.message = "Sorry for inconvenience! Registration failed"}
    );
    this.patientPortalService.setHasAppointment(true); // if hasAppointment is true, button label is changed to Get Appoinment status
  }
  
}
