import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToWaitlistComponent } from './add-to-waitlist.component';

describe('AddToWaitlistComponent', () => {
  let component: AddToWaitlistComponent;
  let fixture: ComponentFixture<AddToWaitlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddToWaitlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToWaitlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
