import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';
import { PatientPortalService } from 'src/app/services/patient-portal.service';
import { AddToWaitlistComponent } from '../add-to-waitlist/add-to-waitlist.component';
import { AppointmentFormComponent } from '../appointment-form/appointment-form.component';
import { AppointmentStatusComponent } from '../appointment-status/appointment-status.component';
import { NewProviderComponent } from '../new-provider/new-provider.component';
import { RegisterComponent } from '../register/register.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // Later our authService will decide which will be true
  // by default these needs to be set to false
  isLoggedIn = false;
  isProvider = false;
  isPatient = false;
  hasAppointment = false;

  img1url: string = "../../assets/doctor_with_vials.jpg";
  img1alt: string = "Doctor Holding Vials";
  img1att1: string = "https://unsplash.com/@hikendal?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText";
  img1att2: string = "Kendal";
  img1att3: string = "https://unsplash.com/s/photos/medical?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText";

  img2url: string = "../../assets/joined_hands.jpg";
  img2alt: string = "Joined Hands";
  img2att1: string = "https://unsplash.com/@hannahbusing?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText";
  img2att2: string = "Hannah Busing";
  img2att3: string = "https://unsplash.com/s/photos/group-hands?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText";
  
  img3url: string = "../../assets/woman_with_stethoscope.jpg";
  img3alt: string = "Woman With Stethoscope";
  img3att1: string = "https://unsplash.com/@jeshoots?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText";
  img3att2: string = "JESHOOTS.COM";
  img3att3: string = "https://unsplash.com/s/photos/medical?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText";
  
  openRegisterCard(){
    this.loginModal.open(RegisterComponent, {panelClass: "mat-dialog-master"});
  }
  openAddToWaitlistCard(){
    this.loginModal.open(AddToWaitlistComponent, {panelClass: "mat-dialog-master"});
  }
  openAppointmentStatusCard(){
    this.loginModal.open(AppointmentStatusComponent, {panelClass: "mat-dialog-master"});
  }
  openNewProviderCard(){
    this.loginModal.open(NewProviderComponent, {panelClass: "mat-dialog-master"});
  }
  openNewAppointmentCard(){
    this.loginModal.open(AppointmentFormComponent, {panelClass: "mat-dialog-master"});
  }
  constructor(private loginModal: MatDialog, private authService: AuthService, private patientPortalService: PatientPortalService) { }

  ngOnInit(): void {
    this.authService.loginStatusObservable.subscribe((isLoggedIn)=>{this.isLoggedIn=isLoggedIn});
    this.authService.isPatientObservable.subscribe((isPatient)=>{this.isPatient=isPatient});
    this.authService.isProviderObservable.subscribe((isProvider)=>{this.isProvider=isProvider});
    this.patientPortalService.hasAppointmentObservable.subscribe((hasAppointment)=>{this.hasAppointment=hasAppointment});
  }

}
