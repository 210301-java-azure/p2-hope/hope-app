import { Component, OnInit } from '@angular/core';
import { Location } from 'src/app/models/location';
import {LocationService} from 'src/app/services/location.service';

import {LocationFormComponent} from 'src/app/components/location-form/location-form.component';

import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

  listOfLocations: Location[] = [];

  message: string = "";

  constructor(private locationModal: MatDialog, private locationService:LocationService) { }

  ngOnInit(): void {
    this.getLocations();
  }

  retrieveLocationForm(){
    this.locationModal.open(LocationFormComponent);
  }

  getLocations(): void {
    this.locationService.getAllLocations()
    .subscribe(data => {
      this.listOfLocations = data;
    });
  }


  deleteLocation(id:number): void {
    this.locationService.deleteLocation(id)
    .subscribe( () => {
      this.message = "Successful delete operation.";
      this.reloadCurrentPage()
    }, 
    () => {
      this.message = "Unsuccessful delete operation.";
    })
  }

  reloadCurrentPage(): void {
    window.location.reload();
  }

}
