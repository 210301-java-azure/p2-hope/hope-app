import { Component, OnInit } from '@angular/core';
import {ProviderData} from 'src/app/models/provider-data';
import {ProviderRegistrationService} from 'src/app/services/provider-registration.service';



@Component({
  selector: 'app-new-provider',
  templateUrl: './new-provider.component.html',
  styleUrls: ['./new-provider.component.css']
})
export class NewProviderComponent implements OnInit {

  newProviderData: ProviderData = new ProviderData();  

  message: string = "";

  constructor(private providerRegistrationService:ProviderRegistrationService) { }

  ngOnInit(): void {
  
  }

  registerProvider() {
    if(!this.newProviderData.providerName || !this.newProviderData.mainAddress || !this.newProviderData.city
      || !this.newProviderData.state || !this.newProviderData.postalCode || !this.newProviderData.mainPhoneNumber 
      || !this.newProviderData.mainEmail || !this.newProviderData.password) {
        this.message = 'Provider registration unsuccessful. Incomplete form.';
        return;
    }
    this.providerRegistrationService.registerProvider(this.newProviderData)
      .subscribe(providerData => this.newProviderData = providerData);

  }
}
