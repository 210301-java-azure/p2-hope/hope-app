import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { from, Observable } from 'rxjs';
import { Appointment } from 'src/app/models/appointment.model';
import { AppointmentFormComponent} from '../appointment-form/appointment-form.component'
import {AppointmentService} from '../../services/appointment.service'
import { identifierModuleUrl } from '@angular/compiler';
import { Router } from '@angular/router';
@Component({
  selector: 'app-appointment-table',
  templateUrl: './appointment-table.component.html',
  styleUrls: ['./appointment-table.component.css']
})
export class AppointmentTableComponent implements OnInit {

 

  appts: Appointment[] =[];
  
  errorMessage: string ="";
  message: string="";
  
  constructor(private apptModal: MatDialog, private apptService: AppointmentService, private router : Router) {
    apptService.getAllData().subscribe(
      (apptsReturned)=>{
        this.appts = apptsReturned;
      },
      ()=>{this.errorMessage = "there was an issue getting your data"}
    )

   }
  
  ngOnInit(): void {
  }

  deleteAppt(id: number){
    this.apptService.deleteAppt(id).subscribe(
        ()=>{ this.message = "successful deleted";      
              this.reloadCurrentRoute();
              this.ngOnInit();
      },
        ()=> { this.message = "fail to delete"}
    )
  }
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }
  
  openAPPTform(){
    this.apptModal.open(AppointmentFormComponent, {panelClass: "mat-dialog-master"});
  }
}
