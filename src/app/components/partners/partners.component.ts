import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { Provider } from 'src/app/models/provider';
import { ProviderRegistrationService} from "../../services/provider-registration.service"
@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.css']
})
export class PartnersComponent implements OnInit {
  providers: Provider[] = [];
  errorMessage: string ="";
  constructor(private providerService: ProviderRegistrationService) {
      providerService.getAllProviders().subscribe(
        (providersReturned)=>{
          this.providers = providersReturned;
        },
        ()=>{this.errorMessage = "there was an issue getting providers" }
      )
   }

  

  ngOnInit(): void {
  }

}
