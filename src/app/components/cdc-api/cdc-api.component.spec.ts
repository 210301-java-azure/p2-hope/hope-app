import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CdcApiComponent } from './cdc-api.component';

describe('CdcApiComponent', () => {
  let component: CdcApiComponent;
  let fixture: ComponentFixture<CdcApiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CdcApiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CdcApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
