import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProviderPortalService {

  providerPortalUrl: string = environment.baseUrl + "providers";
  constructor() { }
}
