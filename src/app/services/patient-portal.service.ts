import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Patient } from '../models/patient';

let token: string = sessionStorage.getItem("token") || "";

const httpOptions = {
  headers: new HttpHeaders().set("Content-Type", "application/json").set("Authorization", token),
  observe: "response"
}

@Injectable({
  providedIn: 'root'
})
export class PatientPortalService {

  patientPortalUrl: string = environment.baseUrl + "patients";

  private hasAppointment = new BehaviorSubject(false);
  hasAppointmentObservable = this.hasAppointment.asObservable();
  

  constructor(private httpClient: HttpClient) { }

  setHasAppointment(hasAppointment: boolean ) {
    this.hasAppointment.next(hasAppointment);
  }

  addPatient(patient: Patient): Observable<any>{
    return this.httpClient.post<Patient>(this.patientPortalUrl, patient, {
      headers: new HttpHeaders().set("Content-Type", "application/json").set("Authorization", token),
      observe: "response"
    });
  }
}
