import { TestBed } from '@angular/core/testing';

import { CdcApiService } from './cdc-api.service';

describe('CdcApiService', () => {
  let service: CdcApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CdcApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
