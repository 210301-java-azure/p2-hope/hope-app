import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddToWaitlistComponent } from './components/add-to-waitlist/add-to-waitlist.component';
import { PatientPortalComponent } from './components/patient-portal/patient-portal.component';
import { ProviderPortalComponent } from './components/provider-portal/provider-portal.component';
import { AppointmentTableComponent } from './components/appointment-table/appointment-table.component';
import { AppointmentFormComponent } from './components/appointment-form/appointment-form.component';
import { PatientPortalService } from './services/patient-portal.service';
import { AppointmentStatusComponent } from './components/appointment-status/appointment-status.component';
import { PartnersComponent } from './components/partners/partners.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { NewsPortalComponent } from './components/news-portal/news-portal.component';
import { HomeComponent } from './components/home/home.component';
import { AuthService } from './services/auth.service';
import { UserComponent } from './components/user/user.component';
import { NewProviderComponent } from './components/new-provider/new-provider.component';
import { CdcAPIComponent } from './components/cdc-api/cdc-api.component';
import { LocationComponent } from './components/location/location.component';
import { LocationFormComponent } from './components/location-form/location-form.component';


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    RegisterComponent,
    LoginComponent,
    AddToWaitlistComponent,
    PatientPortalComponent,
    ProviderPortalComponent,
    AppointmentTableComponent,
    AppointmentFormComponent,
    PatientPortalComponent,
    AppointmentStatusComponent,
    PartnersComponent,
    AboutUsComponent,
    NewsPortalComponent,
    UserComponent,
    HomeComponent,
    NewProviderComponent,
    CdcAPIComponent,
    LocationComponent,
    LocationFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatDialogModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [PatientPortalService,
              AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
